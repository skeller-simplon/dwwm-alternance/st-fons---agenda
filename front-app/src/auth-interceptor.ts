import axios from "axios";
import store from '@/store';

//On ajoute un truc qui interceptera toutes les requêtes de axios pour y ajouter le header Authorization avec notre token dedans
axios.interceptors.request.use(function(config) { 
    if( localStorage.getItem('token')){
        config.headers = {
            Authorization: 'Bearer '+localStorage.getItem('token')
        };
    }
    return config;
});

axios.interceptors.response.use(function(response) {
    return response;
}, function(error) {

    if(error.response && error.response.status === 401) {
        store.dispatch('logout').then(() => {
            if(!window.location.pathname.match(/\/login/)) {
                window.location.href = '/login?origin='+window.location.pathname;
            }
        });
        return error.response;
    }
    return Promise.reject(error);
})