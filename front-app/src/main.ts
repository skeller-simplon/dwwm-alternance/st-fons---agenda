import Vue from 'vue';
import vuetify from '@/plugins/vuetify';
import App from './App.vue';
import router from './router';
import './auth-interceptor.ts';
import store from './store';
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import ToggleButton from 'vue-js-toggle-button'
import DatetimePicker from 'vuetify-datetime-picker'
import VueNoty from 'vuejs-noty'

Vue.use(VueNoty, {
  timeout: 4000,
  progressBar: true,
  layout: 'topCenter'
});
Vue.use(DatetimePicker);
Vue.use(BootstrapVue);
Vue.use(ToggleButton);
Vue.config.productionTip = false;

const ignoreWarnMessage = 'The .native modifier for v-on is only valid on components but it was used on <div>.';
Vue.config.warnHandler = function (msg, vm, trace) {
  // `trace` is the component hierarchy trace
  if (msg === ignoreWarnMessage) {
    msg = null;
    vm = null;
    trace = null;
    trace;
    vm;
  }
}

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app');
