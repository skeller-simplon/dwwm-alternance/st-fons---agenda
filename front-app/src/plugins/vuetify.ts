import Vue from 'vue';
import Vuetify from 'vuetify/lib';

import fr from 'vuetify/src/locale/fr'
Vue.use(Vuetify);

const opts = {
  lang: {
      locales: { fr },
      current: 'fr',
  },
};

export default new Vuetify(opts);
