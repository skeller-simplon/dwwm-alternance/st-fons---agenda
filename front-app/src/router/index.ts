import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Login from '@/views/Authentication/Login.vue';
import AwaitValidation from '@/views/Authentication/AwaitValidation.vue';
import Register from '@/views/Authentication/Register.vue';
import ResetPassword from "@/views/Authentication/ResetPassword.vue";


import Admin from '@/views/Admin/Admin.vue'
import store from '@/store';
import AuthService from '@/services/authService';
Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path:'/await-validation',
    name: 'awaitValidation',
    component: AwaitValidation
  },
  {
    path:'/resetPassword',
    name: 'resetPassword',
    component: ResetPassword
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/admin',
    name: 'admin',
    component: Admin,
    beforeEnter: async (to, from, next) => {
      await new AuthService().GetUserFromToken(localStorage.getItem("token"))
      if(store.state.user && store.state.user.isAdmin) {
        next();
      } else {
        next('/login?origin='+window.location.pathname);
      }
    }
  }
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;