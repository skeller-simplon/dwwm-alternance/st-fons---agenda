import Config from './config';
import axios from 'axios';
import store from '@/store';

export default class AuthService{
    public loginUser(email: string, password: string): Promise<any>{
        const body = {
            email: email,
            password: password,
        }
        return axios.post(Config.AuthRoutes.LoginUser, body).then(res => {
            localStorage.setItem('token', res.data.token);
            store.dispatch('login', res.data.user);
            return res.data.user
        });
    }
    public GetUserFromToken(key: string){
        return axios.get(Config.AuthRoutes.GetUserFromToken + "/" + key)
        .then(res => {
            if(res) {
                store.dispatch('login', res.data)
                return res.data;
            }
        });
    }

   
}