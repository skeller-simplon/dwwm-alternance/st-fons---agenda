const BaseConfig = "/api";

export default {
    UserRoutes: {
        GetAllUsers: BaseConfig + "/users/",
        CreateUser: BaseConfig + "/users/add",
        ValidateUser: BaseConfig + "/users/validate/",
        UnValidateUser: BaseConfig + "/users/unValidate/",
        PromoteUser: BaseConfig + "/users/promote/",
        DemoteUser: BaseConfig + "/users/demote/",
        SendResetMail: BaseConfig + "/users/sendResetMail",
        ResetPassword: BaseConfig + "/users/resetPassword"
    },
    AuthRoutes: {
        LoginUser: BaseConfig + "/login",
        GetUserFromToken: BaseConfig + "/getUserFromToken",
        VerifyEmail: BaseConfig + "/emailTaken/"
    },
    EventRoutes: {
        GetAllEvents: BaseConfig + "/events",
        AddEvent: BaseConfig + "/events/add",
        DeleteEvent: BaseConfig + "/events/delete/",
        UpdateEvent: BaseConfig + "/events/edit/" 
    },
    SettingsRoutes: {
        Settings: BaseConfig + "/settings"
    }
}
