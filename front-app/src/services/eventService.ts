import axios from 'axios';
import config from './config';
import CalendarDate from '../transport/CalendarEvent';
import { User } from '@/transport/User';
import { Event } from '../transport/Event';


export default class EventService {

    async getAll() {
        const response = await axios.get(config.EventRoutes.GetAllEvents);
        // Obligé de rajouter cette ligne pour activer les setters liés à la classe User
        response.data.forEach(event => { event.user = new User(event.user) });
        return response.data.map(e => new CalendarDate(e.startDate, e.endDate, e.title, e));
    }
    public addEvent(event: Event){
        const data = {
            startDate: event.startDate,
            endDate: event.endDate,
            title: event.title,
            description: event.description,

        }
        return axios.post(config.EventRoutes.AddEvent, data).then(res => res)
    }
    public updateEvent(event: Event){
        const data = {
            startDate: event.startDate,
            endDate: event.endDate,
            title: event.title,
            description: event.description,
        }
        return axios.post(config.EventRoutes.UpdateEvent + event._id, data)
    }
    public deleteEvent(eventId: string){
        return axios.get(config.EventRoutes.DeleteEvent + eventId)
    }
}




