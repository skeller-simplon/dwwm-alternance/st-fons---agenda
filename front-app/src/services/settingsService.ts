import axios from 'axios';
import config from './config';
import { Settings } from '../transport/Settings';


export default class SettingsService {

    async getAll() {
        const response = await axios.get(config.SettingsRoutes.Settings);
        return response.data;
    }

    async postSetting(setting: Settings) {
        const body = {
            logo1: setting.logo1,
            logo2: setting.logo2
        }
        const response = await axios.post(config.SettingsRoutes.Settings, body);
        return response.data;

    }
}