import Config from './config';
import axios from 'axios';
import { User } from '@/transport/User';

export default class UserService{
    getAllUsers(): Promise<User[]>{
        return axios.get(Config.UserRoutes.GetAllUsers).then(res => {
            let resp = []
            res.data.forEach(ele => {
                resp.push(new User(ele));
            });
            return resp as Array<User>; 
        }).catch(err => {
            alert('error in getAllUsers' + err.message)
        }) as Promise<User[]>
    }
    createUser(user: User){
        const body = {
            email: user.email,
            password: user.password,
            label: user.label,
            firstName: user.firstName,
            lastName: user.lastName,
            isValid: user.isValid,
            isAdmin: user.isAdmin
        }
        return axios.post(Config.UserRoutes.CreateUser, body)
        .then(response => { 
            return response.data;
        });
    }

    public verifyEmail(email:string):Promise<any> {
        return axios.get(Config.AuthRoutes.VerifyEmail + email)
    }
    
    public validateUser(userId: string){
        return axios.post(Config.UserRoutes.ValidateUser + userId)
    }
    public unValidateUser(userId: string){
        return axios.post(Config.UserRoutes.UnValidateUser + userId)
    }
    public promoteUser(userId: string){
        return axios.post(Config.UserRoutes.PromoteUser + userId)
    }
    public demoteUser(userId: string){
        return axios.post(Config.UserRoutes.DemoteUser + userId)
    }
    public sendResetMail(userEmail:string){
        let body = {
            email: userEmail
        }
        return axios.post(Config.UserRoutes.SendResetMail, body)
    }
    public resetPassword(jwt: string | string[], password: string){
        let body = {
            jwt,
            password
        }
        return axios.post(Config.UserRoutes.ResetPassword, body)
    }

}