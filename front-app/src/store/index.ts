import Vue from 'vue';
import Vuex from 'vuex';
import SettingsService from '../services/settingsService';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: null,
    settings: null
  },
  mutations: {
    CHANGE_USER(state, payload) {
      state.user = payload;
    },
    CHANGE_SETTINGS(state, payload) {
      state.settings = payload;
    }
  },
  actions: {
    login({commit}, user) {
      commit('CHANGE_USER', user);
    },
    logout({commit}) {
      commit('CHANGE_USER', null);
      localStorage.removeItem('token');
    },
    async getSettings({commit}) {
      commit('CHANGE_SETTINGS', await new SettingsService().getAll())
    }
  },
  modules: {
  },
});
