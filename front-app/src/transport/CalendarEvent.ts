import { Event } from './Event';


/**
 * Vuetify calendar utilises un évènement calendrier spécifique. Cette classe permet de gérer cette compatibilité.
 */
export default class CalendarEvent {
    // Date de début.
    public start: string;

    // Date de fin.
    public end: string;

    // Nom de l'évènement.
    public name: string;

    // Objet DateTime comme extrait de la base de donnée.
    public data: Event;

    constructor(startDate: Date, endDate: Date, title: string, data: Event) {
        this.start = this.formatDate(new Date(startDate), true);
        this.end = this.formatDate(new Date(endDate), true);
        this.name = title;
        this.data = data;
    }
    
    /**
     * Transforme un objet date en string utilisable par Vuetify Calendar.
     * @param a Date d'origine
     * @param withTime Si à true, ajoute les heures et les minutes. Permet de rendre des évènements qui durent toute une journée.
     */
    formatDate(a: Date, withTime: boolean): string {
        return withTime
            ? `${a.getFullYear()}-${a.getMonth() + 1}-${a.getDate()} ${a.getHours()}:${a.getMinutes()}`
            : `${a.getFullYear()}-${a.getMonth() + 1}-${a.getDate()}`
    }
}