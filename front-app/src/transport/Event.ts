import { User } from "./User";

export class Event{
    constructor(init?:Partial<Event>){
        Object.assign(this, init);
    }
    public startDate: Date;
    public endDate: Date;
    public title: string;
    public description: string;
    public user: User;
    public _id: string;
}