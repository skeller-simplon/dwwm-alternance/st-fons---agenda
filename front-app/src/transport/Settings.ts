export class Settings{
    constructor(logo1: String, logo2: String){
        this.logo1 = logo1;
        this.logo2 = logo2;
    }
    logo1: String;
    logo2: String;
}