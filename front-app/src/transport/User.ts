import methods from "../utils/methods";
import { Event } from './Event';
export class User {

    public constructor(init?:Partial<User>) {
        Object.assign(this, init);
        this.color = this.computeCssValue(this.displayName)
    }
    public email: string;
    public password: string;
    public label: string;
    public firstName: string;
    public lastName: string;
    public isValid: boolean;
    public isAdmin: boolean;
    public events: Array<Event>;
    public _id: string;
    public color: string;

    public get fullName(): string{
        var r = '';
        if (!methods.isNullOrWhiteSpace(this.lastName)) r += this.lastName
        if (!methods.isNullOrWhiteSpace(this.firstName)) r += ' ' + this.firstName
        return r !== "" ? r : null; 
    }
    get displayName(): string{
        return methods.isNullOrWhiteSpace(this.label) ? this.fullName : this.label;
    }
    public toString(){
        return this.displayName;
    }
    /**
     * Selectionne une couleur depuis une liste de manière prédictive. Cette couleur est définie depuis une string d'input.
     * @param str Une string : nom de l'evenement, utilisateur, etc
     */
    computeCssValue(str: string) {
        let colors = ['blue', 'lime', 'red', 'orange', 'green', 'teal', 'purple', 'pink', 'grey'];
        let val = 0;
        for (var i = 0; i < str.length; i++) {
            val += Math.floor(str.charCodeAt(i))
        }
        while (val > colors.length) {
            val -= colors.length
        }
        return colors[val - 1];
    }
}