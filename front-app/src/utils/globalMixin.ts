import Vue from "vue";
import methods from './methods'
import AuthService from '@/services/authService';

export default Vue.mixin({
  methods
})