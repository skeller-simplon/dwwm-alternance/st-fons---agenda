export default {
    isNullOrWhiteSpace(str: string){
        return str === "" || str === null || str === undefined;
      },
      isNullOrEmpty(str: string | string[]){
        if (typeof str === undefined)
          return true;
        if (typeof str === "string")
          return this.isNullOrWhiteSpace(str.toString());
        else
          return str === undefined || str === null || str === [];
      }
      
}