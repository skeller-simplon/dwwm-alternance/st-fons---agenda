# Agenda Partagé

### Sommaire

1. [Description](#description)
2. [Technologies](#technos)
3. [Installation](#installation)

### Description <a name="description"></a>

Un agenda partagé pour la mairie de St-Fons et ses associations.

### Technologies utilisées <a name="technos"></a>

**Dépendances :**

* *expressjs* - Le serveur de traitement et de routage des requêtes HTTP.
* *express-jwt* - Middleware pour la validation des JWTs pour l'authentification.
* *jsonwebtoken* - Pour générer les JWTs utilisés par l'authentification.
* *mongoose* - Pour modéliser et mapper les données MongoDB en javascript.
* *mongoose-unique-validator* - Pour traiter les erreurs de validation uniques dans Mongoose. <br>Mongoose ne gère la validation qu'au niveau du document, donc un index unique à travers une collection lancera une exception au niveau du pilote. Le plugin mongoose-unique-validator nous aide en formatant l'erreur comme une erreur normale mongoose ValidationError.
* *passport* - Pour le traitement de l'authentification des utilisateurs.
* *slug* - Pour encoder les titres dans un format URL convivial.

### Installation <a name="installation"></a>

Pour installer les dépendances du projet et démarrer le serveur,

```
cd ...
npm install
sudo service mongod start
npm start

Si port déjà utilisé : 
sudo lsof -i :4000
kill -9 {PID}
```
