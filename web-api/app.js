const express = require('express');
const app = express();
const router = require('./routes/routes')
const history = require('connect-history-api-fallback');
require('dotenv').config();
const DB_port = process.env.PORT || process.env.DB_port;
const mongoUrl = process.env.MONGO_URL || "mongodb://localhost/st-fons-calendar?authSource=admin";
const mongoose = require('mongoose')
mongoose.set('useCreateIndex', true);

mongoose.connect(mongoUrl, { useNewUrlParser: true })

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log(`[MongoDB is running here 🎯 ]`);
});

//Auth
const morgan = require('morgan');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const cors = require('cors');

app.use(express.urlencoded({extended: true}));
app.use(express.json({limit: '50mb'}));

app.use(cors())
// Initialisation de passport
app.use(passport.initialize());

// On appelle notre stratégie 
require('./config/passport')(passport);

app.use(express.static('public'));

app.use(history({
    disableDotRule: true,
    verbose: true,
    rewrites: [
        {
          from: /^\/api\/.*$/,
          to: function(context) {
              return context.parsedUrl.path
          }
        }
      ]
}));
app.use(express.static('public'));


// Log les requêtes
app.use(morgan('dev'));

// Route d'accueil
// app.get('/', function (req, res) {
//     res.send('Home');
// });

app.use(function(req, res, next) {
    if(req.url.includes("localhost")){
        console.log("HIT TARGET")
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    }
    next();
  });

app.use('/', router)


app.listen(DB_port, () => {
    console.log([`App tourne sur : ${DB_port}`]);
});