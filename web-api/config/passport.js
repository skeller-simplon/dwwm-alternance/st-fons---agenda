// var JwtStrategy = require('passport-jwt').Strategy;  
// var ExtractJwt = require('passport-jwt').ExtractJwt;  
var UserModel = require('../db/models/user');
const bcrypt = require('bcrypt');
// var config = require('./main');


var passport = require('passport')
const LocalStrategy = require('passport-local').Strategy;

module.exports = passport => passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
},
  function (email, password, cb) {
    console.log(`user: ${email}, pass: ${password}`)
    //this one is typically a DB call. Assume that the returned user object is pre-formatted and ready for storing in JWT

    return UserModel.findOne({ email })
      .then(user => {
        
        if (user == null) {
          return cb("Utilisateur.ice inconnu.e", null);
        }
        bcrypt.compare(password, user.password, function (err, same) {
          //  Return d'erreur
          if (err) {
            return cb(err, null);
          }
          // Mot de passe ne match pas
          if (!same) {
            return cb("Les mots de passe ne sont pas les mêmes.", null, { message: 'Les mots de passe ne correspondent pas' });
          }
          // Succès
          return cb(null, user)
        })

        // return cb(null, user, { message: 'Logged In Successfully' });
      })
      .catch(err => cb(err));
  }
));

const JwtStrategy = require('passport-jwt').Strategy,
  ExtractJwt = require('passport-jwt').ExtractJwt;
let opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('Bearer');
opts.secretOrKey = 'secret';
passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
  
  UserModel.findOne({ email:jwt_payload.data.email }, function (err, user) {
    
    if (err) {
      return done(err, false);
    }
    if (user) {
      if(!user.isValid) {
        return done(null, false);
      }
      return done(null, user);
    } else {
      return done(null, false);
      // or you could create a new account
    }
  });
}));