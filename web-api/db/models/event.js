const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const eventSchema = new Schema({
    startDate: {
        type: Date,
        required: true
    },
    endDate: {
        type: Date,
        required: true
    },
    title: {
        type: String,
        require: true
    },
    description: {
        type: String,
        require: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    }
});

const Event = mongoose.model('Event', eventSchema);

module.exports = Event;
