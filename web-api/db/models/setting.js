const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const settingSchema = new Schema({
    
    logo1: {
        type: String,
        require: false
    },
    logo2: {
        type: String,
        require: false
    }
});

const Setting = mongoose.model('Setting', settingSchema);

module.exports = Setting;
