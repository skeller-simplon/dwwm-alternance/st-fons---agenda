// Connection
const mongoose = require('mongoose');  
const bcrypt = require('bcrypt');
mongoose.connection;

let UserSchema = new mongoose.Schema({  
  email: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  label: {
    type: String
  },
  firstName: {
    type: String
  },
  lastName: {
    type: String
  },
  isValid: {
    type: Boolean,
    default: false
  },
  isAdmin: {
    type: Boolean,
    default: false
  },
  events: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Event"
  }]
});

// Enregistrement de l'utilisateur (toujours hasher les mots de passe en production) 
UserSchema.pre('save', function (next) {  
  let user = this;
  if (this.isModified('password') || this.isNew) {
    bcrypt.genSalt(10, function (err, salt) {
      if (err) {
        return next(err);
      }
      bcrypt.hash(user.password, salt, function(err, hash) {
        if (err) {
          return next(err);
        }
        user.password = hash;
        next();
      });
    });
  } else {
    return next();
  }
});

// Comparaison des mots de passes reçus et en base
UserSchema.methods.comparePassword = function(pw, cb) {  
  bcrypt.compare(pw, this.password, function(err, isMatch) {
    if (err) {
      return cb(err);
    }
    cb(null, isMatch);
  });
};

module.exports = mongoose.model('User', UserSchema);  