const express = require('express');
const AuthRouter = express.Router();
const User = require('../../db/models/user');
const passport = require('passport');
const jwt = require('jsonwebtoken');


// Enregistrement
AuthRouter.post('/register', function (req, res) {
    if (!req.body.email || !req.body.password) {
        res.json({ success: false, message: 'Please enter email and password.' });
    } else {
        var newUser = new User({
            email: req.body.email,
            password: req.body.password,
            label: req.body.label,
            firstName: req.body.firstName,
            lastName: req.body.lastName

        });

        //  Création de l'utilisateur
        newUser.save(function (err) {
            if (err) {
                return res.json({ success: false, message: 'Email déja utilisé' });
            }
            res.json({ success: true, message: 'Créé ! ' });
        });
    }
});
AuthRouter.post('/login', function (req, res, next) {
    passport.authenticate('local', {session: false}, (err, user, info) => {
        if (err || !user) {
            console.log(err)
            return res.status(400).json({
                message: 'ERROR FROM WEB API',
                user   : user,
                err: err
            });
        }
       req.login(user, {session: false}, (err) => {
           if (err) {
               return res.send(err);
           }
           // generate a signed son web token with the contents of user object and return it in the response
           const token = jwt.sign(
                { data: user },
                'secret', {
                    expiresIn: '1h'
                });
           return res.json({user, token});
        });
    })(req, res);
});

AuthRouter.get('/getUserFromToken/:token', function (req, res) {
    try{
        const validated = jwt.verify(req.params.token, "secret")
        User.findById(validated.data._id).populate("events").then(response => {
            res.send(response);
        })
    
    }catch(ex){
        res.status(401).send(ex)
    }
});

AuthRouter.get('/emailTaken/:email', function(req, res) {
    User.findOne({email: req.params.email}).then(user => {
        if(user) {
            res.status(403).send();
        } else {
            res.status(204).send();
        }
    }).catch(() => res.status(500).send());
})

module.exports = AuthRouter;