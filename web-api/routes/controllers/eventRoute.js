const express = require('express');
const Event = require('../../db/models/event');
const User = require('../../db/models/user');
const { authorizedAction } = require('../../utils/roles');
const passport = require('passport');
const validator = require('validator');

const EventRouter = express.Router();


EventRouter.get('/', (req, res) => {
    Event.find({}).populate('user')
        .then((events) => res.json(events))
        .catch((err) => res.json(err));
});

EventRouter.post('/add', passport.authenticate('jwt', { session: false }), (req, res) => {

    if (!validator.isEmpty(req.body.startDate) &&
        !validator.isEmpty(req.body.endDate) &&
        !validator.isEmpty(req.body.title) &&
        !validator.isEmpty(req.body.description)
    ) {
        if (new Date(req.body.startDate) > Date.now() &&
            req.body.endDate > req.body.startDate
        ) {
            const newEvent = new Event(req.body);
            newEvent.user = req.user._id;
            newEvent.save((err, event) => {
                if (err) console.error(err)
                res.redirect('/api/events');
            })
        }else {
            res.status(400).json({message: "Les dates de début et de fin ne peuvent pas être les mêmes"})
        }
    } else {
        res.status(400).json({ message: 'Les champs sont incorrects' })
    }


});

EventRouter.get('/delete/:id', passport.authenticate('jwt', { session: false }), authorizedAction('event'), (req, res) => {
    Event.deleteOne({ _id: req.params.id }, (err, event) => {
        if (err) console.error(err)
        res.redirect('/api/events')
    })

});

EventRouter.route('/:id')
    .get((req, res) => {
        Event.findById(req.params.id).populate('user')
            .then((event => res.json({ event: event })))
    })
    
EventRouter.route('/edit/:id')
    .post(passport.authenticate('jwt', { session: false }), authorizedAction('event'), (req, res) => {
        try{
            Event.findById({ _id: req.params.id }, (err, event) => {
                if (err) console.error(err)
                if (!validator.isEmpty(req.body.startDate) &&
                    !validator.isEmpty(req.body.endDate) &&
                    !validator.isEmpty(req.body.title) &&
                    !validator.isEmpty(req.body.description)
                ) {
                    if (new Date(req.body.startDate) < Date.now() &&
                        req.body.endDate < req.body.startDate
                    ){
                        res.status(400).json({message: "Les dates de début et de fin ne peuvent pas être les mêmes"})
                    }
                    Object.assign(event, req.body).save((err, event) => {
                        if (err) console.error(err)
                        res.redirect('/api/events');
                    })
                } else {
                    res.status(400).json({ message: 'Les champs sont incorrects' })
                }
            })
        }catch{
            res.status(400).json({ message: 'Une erreur est survenue' })
        }
        
    })

module.exports = EventRouter;