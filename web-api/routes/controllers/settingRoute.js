const express = require('express');
const Setting = require('../../db/models/setting');

const SettingRouter = express.Router();

SettingRouter.get('/', (req, res) => {
    Setting.findOne({})
        .then((settings) => res.json(settings))
        .catch(err => res.json(err))
})

SettingRouter.post('/', (req, res) => {

    Setting.findOne({}, (err, settings) => {
        if (!settings) {
            const newSetting = new Setting(req.body)
            newSetting.save((err, setting) => {
                if (err) console.error(err)
                res.json(newSetting);
            })
        } else {
            Object.assign(settings, req.body).save((err, setting) => {
                if (err) console.error(err)
                res.json(setting);
            })
        }
    })

})


module.exports = SettingRouter;