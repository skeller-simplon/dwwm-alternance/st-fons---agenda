const express = require('express')
const User = require('../../db/models/user')
const validator = require('validator');
const { authorizedAction, checkIsAdmin } = require('../../utils/roles');
const passport = require('passport');
const UserRouter = express.Router();
const sendGridMail = require("@sendgrid/mail");
const jwt = require('jsonwebtoken');


sendGridMail.setApiKey('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');

/**
 * Retrieve user's list.
 */
UserRouter.get('/', passport.authenticate('jwt', { session: false }), checkIsAdmin(), (req, res) => {
    User.find({}).populate('events')
        .then((users) => {
            res.json(users)
        })
        .catch(err => res.json(err))
})

/**
 * Create new user.
 */
UserRouter.post('/add', (req, res) => {
    delete req.body.isValid;
    delete req.body.isAdmin;
    if (validator.isEmail(req.body.email) &&
        validator.isLength(req.body.password, { min: 5 }) &&
        (req.body.label !== null || (req.body.lastName !== null && req.body.firstName !== null))) {
        User.findOne({ email: req.body.email }).then(user => {
            if (user) {
                res.status(400).json({ message: 'Email déjà utilisé' });
                return;
            }
            const newUser = new User(req.body);
            newUser.save((err, user) => {
                if (err) {
                    console.log(err);
                    res.json({ message: err.message });
                }
                return res.json(newUser);
            })
        });

        const emailMessage = {
            to: process.env.ADMIN_EMAIL,
            from: process.env.ADMIN_EMAIL,
            subject: `Nouvel utilisateur`,
            text: `Nouvel utilisateur`,
            html: `<p>Bonjour, </p><p>Un nouvel utilisateur souhaite rejoindre St-fons Agenda avec cette adresse e-mail: ${req.body.email}, </p><p>veuillez confirmer son inscription: <a href= '${req.hostname}/admin'>Confirmation admin</a></p>`
        };
        sendGridMail.send(emailMessage)
            .then(response => console.log(response))
            .catch(reason => console.log(reason));
    } else {
        res.status(400).json({ message: 'Les champs sont incorrects' });
    }
})

/**
 * Retrieve/Modify a specific user.
 */
UserRouter.route('/one/:id')
    .get(passport.authenticate('jwt', { session: false }), authorizedAction(), (req, res) => {
        User.findById(req.params.id)
            .populate('events')
            .then(user => res.json({ user: user }))
            .catch(err => res.json(err))
    })
    .post(passport.authenticate('jwt', { session: false }), authorizedAction(), (req, res) => {
        User.findById({ _id: req.params.id }, (err, user) => {
            if (err) console.error(err)
            delete req.body.isValid;
            delete req.body.isAdmin;
            delete req.body.email;
            if (validator.isEmail(req.body.email) &&
                validator.isLength(req.body.password, { min: 5 }) &&
                (req.body.label !== null || (req.body.lastName !== null && req.body.firstName !== null))) {

                Object.assign(user, req.body).save((err, user) => {
                    if (err) console.error(err)
                    res.redirect('/api/users')
                })
            } else {
                res.status(400).json({ message: 'Les champs sont incorrects' });
            }
        })
    })

/**
 * Delete a user.
 */
UserRouter.get('/delete/:id', passport.authenticate('jwt', { session: false }), authorizedAction(), (req, res) => {
    User.deleteOne({ _id: req.params.id }, (err, user) => {
        if (err) console.error(err)
        res.redirect('/api/users')
    })
})

/**
 * Validate a user.
 */
UserRouter.post("/validate/:id", passport.authenticate('jwt', { session: false }), checkIsAdmin(), (req, res) => {
    User.findById({ _id: req.params.id }, (err, user) => {
        if (err) return res.status(401).send();
        user.isValid = true;
        user.save();

        const emailMessage = {
            to: user.email,
            from: process.env.ADMIN_EMAIL,
            subject: `Validation St-Fons Agenda`,
            text: `Validation St-Fons agenda`,
            html: `<p>Bonjour, </p> <p>votre inscription à St-Fons agenda a été validé.</p><a href= ${req.hostname}/login>Cliquer ici pour vous rendre sur le site</a>`
        };

        sendGridMail.send(emailMessage)
            .then(response => console.log(response))
            .catch(reason => console.log(reason));

        res.status(204).send();
    })
})

/**
 * Un-validate a user.
 */
UserRouter.post("/unValidate/:id", passport.authenticate('jwt', { session: false }), checkIsAdmin(), (req, res) => {
    User.findById({ _id: req.params.id }, (err, user) => {
        if (err) return res.status(401).send();
        user.isValid = false;
        user.save();
        res.status(204).send();
    })
})

/**
 * Promote a user.
 */
UserRouter.post("/promote/:id", passport.authenticate('jwt', { session: false }), checkIsAdmin(), (req, res) => {
    User.findById({ _id: req.params.id }, (err, user) => {
        if (err) return res.status(401).send();
        user.isAdmin = true;
        user.save();
        res.status(204).send();
    })

})

/**
 * Demote a user.
 */
UserRouter.post("/demote/:id", passport.authenticate('jwt', { session: false }), checkIsAdmin(), (req, res) => {
    User.findById({ _id: req.params.id }, (err, user) => {
        if (err) return res.status(401).send();
        user.isAdmin = false;
        user.save();
        res.status(204).send();
    })
})

UserRouter.post('/sendResetMail', function (req, res) {

    User.findOne({ email: req.body.email }).then(user => {
        if (!user) {
            res.status(403).send();
        }
        const token = jwt.sign(
            {
                userId: user._id,
                userEmail: user.email,
                reset: true
            },
            'secret', {
            expiresIn: '1h'
        });
        const messageEmail = {
            to: req.body.email,
            from: process.env.ADMIN_EMAIL,
            subject: "Réinitialisation du mot de passe",
            html: `<p>Bonjour,</p><p>pour changer votre mot de passe, veuillez cliquer sur le lien suivant:&nbsp;</p><p><a href='${req.hostname}/resetPassword?jwt=${token}'>Réinitialiser votre mot de passe</a></p><p>Cordialement.</p>`
        };
        sendGridMail.send(messageEmail)
            .then(response => console.log(response))
            .catch(reason => console.log(reason));
        res.json({ message: 'ok' })
    })
})

UserRouter.post('/resetPassword', function (req, res) {
    try {
        // Rempli d'un objet userId, userEmail, et reset si valide. Sinon throw.
        const validated = jwt.verify(req.body.jwt, "secret")
        if (validated.reset) {
            User.findOne({ _id: validated.userId })
                .then(user => {
                    user.password = req.body.password;
                    user.save();
                    res.status(204).send();
                })
        }
        res.status(202)
    }
    catch (ex) {
        res.status(400).send(ex)
    }
});
module.exports = UserRouter;
