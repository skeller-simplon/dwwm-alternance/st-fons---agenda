const express = require('express')
const router = express()
const AuthRouter = require('./controllers/authRoute')
const EventRouter = require('./controllers/eventRoute')
const UserRouter = require('./controllers/userRoute')
const SettingRouter = require('./controllers/settingRoute')

const routePrefix = '/api'
router.use(routePrefix + '/', AuthRouter)
router.use(routePrefix + '/events', EventRouter)
router.use(routePrefix + '/users', UserRouter)
router.use(routePrefix + '/settings', SettingRouter)

module.exports = router;
