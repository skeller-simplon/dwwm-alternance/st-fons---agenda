const Event = require('../db/models/event');

const checkIsAdmin = () => (req, res, next) => {

  if (!req.user || !req.user.isAdmin) {
    res.status(401).json({ message: "Veuillez vous connecter en tant qu'admin" })
    return;
  }
  next();
};

const authorizedAction = (type = 'user') => (req, res, next) => {

  if (!req.user.isAdmin) {
    if (type === 'user') {
      if(req.params.id !== req.user._id) {
        res.status(403).json({ message: "Action non autorisée" });
        return;
      }
    } else {
     Event.exists({ user: req.user._id }).then(exists => {
       if(!exists) {
        res.status(403).json({ message: "Action non autorisée" });
        return;
       }
     });
    }
  }
 
  next();
}


module.exports = {
  checkIsAdmin,
  authorizedAction
};